# Panzer IV Ausführung H

This is a model I created back in 2019. I learned a lot while modeling this tank.

## Previews
![Preview](release/Preview.jpg)
![Preview with Ground](release/Preview_with_ground.jpg)
![Preview Wireframe](release/Wireframe.jpg)

## License
Panzer IV by 2tefan is licensed under [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
